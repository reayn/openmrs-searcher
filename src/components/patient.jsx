import React, { Component } from "react";
import "../patient.css";
import { Redirect } from "react-router-dom";
import axios from "axios";

class Patient extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
    };
  }

  componentDidMount() {
    this.setState({
      user: JSON.parse(localStorage.getItem("user")),
    });
  }

  getPatient = (user) => {
    let user_uuid = this.props.match.params.uuid;

    axios
      .get(
        "http://localhost:8080/openmrs/ws/rest/v1/patient/" + user_uuid,
        {},
        {
          auth: {
            username: user.username,
            password: user.password,
          },
        }
      )
      .then((result) => {
        if (result.uuid === user_uuid) {
          this.user = result.person;
        } else {
          console.log("authentication error");
        }
      })
      .catch((e) => {
        console.log("basic auth exception " + e);
      });
  };

  render() {
    if (!this.user) {
      return <Redirect to="/login" />;
    }

    return (
      <div id="patientsInfo" className="container bootstrap snippets bootdey">
        <div className="row ng-scope">
          <div className="col-md-4">
            <div className="panel panel-default">
              <div className="panel-body text-center">
                <div className="pv-lg">
                  <img
                    className="center-block img-responsive img-circle img-thumbnail thumb96"
                    src="https://bootdey.com/img/Content/avatar/avatar1.png"
                    alt="Contact"
                  />
                </div>
                <h3 className="m0 text-bold">{this.user.display}</h3>
              </div>
            </div>
          </div>
          <div className="col-md-8">
            <div className="panel panel-default">
              <div className="panel-body">
                <div className="row pv-lg text-left">
                  <div className="col-lg-12">
                    <div>
                      <label className="col-sm-2">Name</label>
                      <div className="col-sm-10">{this.user.display}</div>
                    </div>
                    <div>
                      <label className="col-sm-2">Age</label>
                      <div className="col-sm-10">{this.user.age}</div>
                    </div>
                    <div>
                      <label className="col-sm-2">Gender</label>
                      <div className="col-sm-10">{this.user.gender}</div>
                    </div>
                    <div>
                      <label className="col-sm-2">Birth Date</label>
                      <div className="col-sm-10">{this.user.birthdate}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Patient;
