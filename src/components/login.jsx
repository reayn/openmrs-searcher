import React, { Component } from "react";
import "../index.css";
import axios from "axios";
import { Redirect } from "react-router-dom";

class Login extends Component {
  constructor(props) {
    super(props);
    this.postLogin = this.postLogin.bind(this);

    this.state = {
      username: "",
      password: "",
    };

    this.logout();
  }

  logout() {
    localStorage.removeItem("user");
  }

  postLogin = (e) => {
    this.setState({ submitted: true });
    const { username, password } = this.state;

    if (!(username && password)) {
      return;
    }

    this.setState({ loading: true });

    axios
      .get(
        "http://localhost:8080/openmrs/ws/rest/v1/session",
        {
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "X-Requested-With",
          },
        },
        {
          auth: {
            username: username,
            password: password,
          },
        }
      )
      .then((result) => {
        console.log(result);
        if (result.authenticated === true) {
          localStorage.setItem(
            "user",
            JSON.stringify(window.btoa(username + ":" + password))
          );
          <Redirect to="/search" />;
        } else {
          console.log("authentication error");
        }
      })
      .catch((e) => {
        console.log("basic auth exception " + e);
      });

    e.preventDefault();
    e.stopPropagation();
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  render() {
    const { username, password } = this.state;

    return (
      <div className="auth-wrapper">
        <div className="auth-inner">
          <form>
            <h3>Sign In</h3>

            <div className="form-group">
              <label>Username</label>
              <input
                required
                name="username"
                type="text"
                value={username}
                className="form-control"
                onChange={this.handleChange}
                placeholder="Enter username"
              />
            </div>

            <div className="form-group">
              <label>Password</label>
              <input
                required
                name="password"
                type="password"
                value={password}
                className="form-control"
                onChange={this.handleChange}
                placeholder="Enter password"
              />
            </div>

            <button
              onClick={this.postLogin}
              type="submit"
              className="btn btn-primary btn-block"
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Login;
