import React, { Component } from "react";
import "../search.css";
import axios from "axios";
import { Redirect } from "react-router-dom";
import ReactTable from "react-table";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      patients: {},
    };
  }

  componentDidMount() {
    this.setState({
      user: JSON.parse(localStorage.getItem("user")),
    });
  }

  getPatients = (user) => {
    axios
      .get(
        "http://localhost:8080/openmrs/ws/rest/v1/patient",
        {},
        {
          auth: {
            username: user.username,
            password: user.password,
          },
        }
      )
      .then((result) => {
        if (result.authenticated === true) {
          this.setState({ loading: false, patients: result.results });
        } else {
          console.log("authentication error");
        }
      })
      .catch((e) => {
        console.log("basic auth exception " + e);
      });
  };

  render() {
    if (!this.user) {
      return <Redirect to="/login" />;
    }

    const columns = [
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Gender",
        accessor: "gender",
      },
      {
        Header: "Age",
        accessor: "age",
      },
    ];

    return (
      <div id="patientsSearch" className="container">
        <div className="row justify-content-center">
          <div className="col-12 col-md-10 col-lg-8">
            <form className="card card-sm">
              <div className="card-body row no-gutters align-items-center">
                <div className="col-auto">
                  <i className="fas fa-search h4 text-body"></i>
                </div>
                <div className="col">
                  <input
                    className="form-control form-control-lg form-control-borderless"
                    type="search"
                    placeholder="Search patients"
                  />
                </div>
                <div className="col-auto">
                  <button
                    onClick={this.getPatients}
                    className="btn btn-lg btn-success"
                    type="submit"
                  >
                    Search
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>

        <ReactTable data={this.state.users} columns={columns} />
      </div>
    );
  }
}

export default Search;
